# Tableau Auto Refresh Extension

Tableau Desktop Extension to automatically refresh the datasources of a Tableau
Dashboard at a regular interval during editing and viewing.

### Pre-requisites

* Tableau Desktop 2018.2 or above

## Installation


- The stable version of the extension can be downloaded from the
  [Tableau Extension Gallery](https://extensiongallery.tableau.com/).

- If you prefer to run the bleeding edge, download or clone this repository and
  import the `autorefresh.trex` file.

After downloading, the extension can be installed in the regular way (select
"Extension" from the Dashboard elements palette and either double-click or drag
it to a proper location on the dashboard.

After a successful installation, you should have a countdown in your dashboard:

![Tableau Desktop Auto-refresh preview](docs/fig1.png)


## Configuration

Selecting the "Configuration" option from the dropdown menu:

![Tableau Desktop Auto-refresh preview](docs/fig2.png)

Should bring up the configuration dialog of the extension, where the length of the countdown timer and the font size can be set.

![Tableau Desktop Auto-refresh preview](docs/fig3.png)




